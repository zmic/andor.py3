#include "stdafx.h"

std::vector<PyMethodDef> v_method_defs;

void set_python_error(PyObject* exc_type, char * format, ...)
{
	char buffer[256];
	va_list args;
	va_start(args, format);
	vsnprintf(buffer, 255, format, args);
	PyErr_SetString(exc_type, buffer);
	va_end(args);
}


template<typename ptr_type, NPY_TYPES expected_type>
bool check_numpy_array(const char* name, PyArrayObject *obj, ptr_type& data)
{
	if (!PyArray_Check(obj))
	{
		set_python_error(PyExc_ValueError, "%s should be numpy array", name);
		return false;
	}
	if (PyArray_TYPE(obj) != expected_type)
	{
		set_python_error(PyExc_ValueError, "%s is numpy array of wrong type", name);
		return false;
	}
	if (PyArray_NDIM(obj) != 1)
	{
		set_python_error(PyExc_ValueError, "%s should be 1-dimensional", name);
		return false;
	}
	data = (ptr_type)PyArray_DATA(obj);
	return true;
}


#if PY_MAJOR_VERSION == 3
int init_functions()
{
	import_array();
	return 0;
}
#else
void init_functions()
{
	import_array();
}
#endif


#if PY_MAJOR_VERSION == 3
PyObject* pyint(int v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(long v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(unsigned int v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(unsigned long v)
{
	return PyLong_FromLong(v);
}
#else
PyObject* pyint(int v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(long v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(unsigned int v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(unsigned long v)
{
	return PyInt_FromLong(v);
}
#endif

bool is_error(int e);
void set_python_error(PyObject* exc_type, char * format, ...);


///////////////////////////////////////////////////////////////////////////////////////////
#define CHECK_ARGS(...)\
do {\
if (!PyArg_ParseTuple(args, __VA_ARGS__))\
{\
	goto error;\
}\
} while (0)



///////////////////////////////////////////////////////////////////////////////////////////
template<typename POINTER2FUNCTION>
PyObject* py_wrapper_simple(POINTER2FUNCTION pf);

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(void))
{
	unsigned int error = pf();
	return pyint(error);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(int*))
{
	int value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(long*))
{
	long value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(unsigned long*))
{
	unsigned long value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*))
{
	float value = 0;
	int error = (pf(&value));
	return Py_BuildValue("if", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(double*))
{
	double value = 0;
	int error = (pf(&value));
	return Py_BuildValue("id", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(int*, int*))
{
	int i1 = 0;
	int i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(long*, long*))
{
	long i1 = 0;
	long i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(unsigned long*, unsigned long*))
{
	unsigned long i1 = 0;
	unsigned long i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	int error = (pf(&f1, &f2));
	return Py_BuildValue("iff", error, f1, f2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	float f3 = 0;
	int error = (pf(&f1, &f2, &f3));
	return Py_BuildValue("ifff", error, f1, f2, f3);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*, float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	float f3 = 0;
	float f4 = 0;
	int error = (pf(&f1, &f2, &f3, &f4));
	return Py_BuildValue("iffff", error, f1, f2, f3, f4);
}

int push_NOARGS(const char* name, PyObject* (*pf)(PyObject*))
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_NOARGS, NULL };
	v_method_defs.push_back(a);
	return 0;
}

#define PUSH_NOARGS(name)\
namespace PYNS_##name\
{\
    int i = push_NOARGS( #name, py__##name );\
}

#define NOARGS(api_function)\
PyObject* py__##api_function(PyObject* self);\
PUSH_NOARGS(api_function)\
PyObject* py__##api_function(PyObject* self)\
{\
	return py_wrapper_simple(api_function);\
}\

///////////////////////////////////////////////////////////////////////////////////////////
template<typename POINTER2FUNCTION>
PyObject* py_wrapper_simple(PyObject* args, POINTER2FUNCTION pf);

int push_VARARGS(const char* name, PyObject* (*pf)(PyObject*, PyObject*))
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_VARARGS, NULL };
	v_method_defs.push_back(a);
	return 0;
}

#define PUSH_VARARGS(name)\
namespace PYNS_##name\
{\
    int i = push_VARARGS( #name, py__##name );\
}

#define VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return py_wrapper_simple(args, api_function);\
}\

///////////////////////////////////////////////////////////////////////////////////////////
//
//  specializations of py_wrapper_simple 
//

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int))
{
	int i;
	CHECK_ARGS("i", &i);
	int error = (pf(i));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(char))
{
	char c;
	CHECK_ARGS("b", &c);
	int error = (pf(c));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(float))
{
	float f;
	CHECK_ARGS("f", &f);
	int error = (pf(f));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(double))
{
	double d;
	CHECK_ARGS("d", &d);
	int error = (pf(d));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(char*))
{
	char* s;
	CHECK_ARGS("s", &s);
	int error = (pf(s));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int))
{
	int i1;
	int i2;
	CHECK_ARGS("ii", &i1, &i2);
	int error = (pf(i1, i2));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int))
{
	int i1;
	int i2;
	int i3;
	CHECK_ARGS("iii", &i1, &i2, &i3);
	int error = (pf(i1, i2, i3));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int))
{
	int i1, i2, i3, i4, i5;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5);
	int error = (pf(i1, i2, i3, i4, i5));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int, int))
{
	int i1, i2, i3, i4, i5, i6;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5, &i6);
	int error = (pf(i1, i2, i3, i4, i5, i6));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int, int, int))
{
	int i1, i2, i3, i4, i5, i6, i7;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5, &i6, &i7);
	int error = (pf(i1, i2, i3, i4, i5, i6, i7));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, float))
{
	int i1;
	float f2;
	CHECK_ARGS("if", &i1, &f2);
	int error = (pf(i1, f2));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int*))
{
	int i;
	int i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int*, int*))
{
	int i0, i1, i2;
	int io0 = 0, io1 = 0;
	CHECK_ARGS("iii", &i0, &i1, &i2);
	int error = (pf(i0, i1, i2, &io0, &io1));
	return Py_BuildValue("iii", error, io0, io1);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(long, long*))
{
	long i;
	long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, long*))
{
	int i;
	long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}


template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(unsigned long, unsigned long*))
{
	unsigned long i;
	unsigned long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, float*))
{
	int i;
	float f_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &f_out));
	return Py_BuildValue("if", error, f_out);
error:
	return 0;
}



///////////////////////////////////////////////////////////////////////////////////////////

template<typename ptr_type, NPY_TYPES expected_type>
PyObject* __py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(int, ptr_type, int))
{
	PyArrayObject* array_object;
	int i0;
	int size;
	ptr_type data;
	CHECK_ARGS("iOi", &i0, &array_object, &size);
	if (!check_numpy_array<ptr_type, expected_type>("First argument", array_object, data))
	{
		goto error;
	}
	int error = (pf(i0, data, size));
	return pyint(error);
error:
	return 0;
}

template<typename ptr_type>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(int, ptr_type, int));


template<>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(int, float*, int))
{
	return __py_wrapper_array_and_size<float*, NPY_FLOAT32>(args, pf);
}


#define INT_ARRAY_AND_SIZE(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return py_wrapper_array_and_size(args, api_function);\
}\


///////////////////////////////////////////////////////////////////////////////////////////



//unsigned int WINAPI ShamrockInitialize(char * IniPath);
VARARGS(ShamrockInitialize)
//unsigned int WINAPI ShamrockClose(void);
NOARGS(ShamrockClose)
//unsigned int WINAPI ShamrockGetNumberDevices(int *nodevices);
NOARGS(ShamrockGetNumberDevices)
//unsigned int WINAPI ShamrockGetFunctionReturnDescription(int error, char *description, int MaxDescStrLen);
//unsigned int WINAPI ShamrockGetSerialNumber(int device, char *serial);
//unsigned int WINAPI ShamrockEepromGetOpticalParams(int device, float *FocalLength, float *AngularDeviation, float *FocalTilt);
//unsigned int WINAPI ShamrockSetGrating(int device, int grating);
VARARGS(ShamrockSetGrating)
//unsigned int WINAPI ShamrockGetGrating(int device, int *grating);
VARARGS(ShamrockGetGrating)
//unsigned int WINAPI ShamrockWavelengthReset(int device);
VARARGS(ShamrockWavelengthReset)
//unsigned int WINAPI ShamrockGetNumberGratings(int device, int *noGratings);
VARARGS(ShamrockGetNumberGratings)
//unsigned int WINAPI ShamrockGetGratingInfo(int device, int Grating, float *Lines, char* Blaze, int *Home, int *Offset);
//unsigned int WINAPI ShamrockSetDetectorOffset(int device, int offset);
//unsigned int WINAPI ShamrockGetDetectorOffset(int device, int *offset);
//unsigned int WINAPI ShamrockSetDetectorOffsetPort2(int device, int offset);
//unsigned int WINAPI ShamrockGetDetectorOffsetPort2(int device, int *offset);
//unsigned int WINAPI ShamrockSetDetectorOffsetEx(int device, int entrancePort, int exitPort, int offset);
//unsigned int WINAPI ShamrockGetDetectorOffsetEx(int device, int entrancePort, int exitPort, int *offset);
//unsigned int WINAPI ShamrockSetGratingOffset(int device, int Grating, int offset);
//unsigned int WINAPI ShamrockGetGratingOffset(int device, int Grating, int *offset);
//unsigned int WINAPI ShamrockGratingIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetTurret(int device, int Turret);
//unsigned int WINAPI ShamrockGetTurret(int device, int *Turret);
//unsigned int WINAPI ShamrockSetWavelength(int device, float wavelength);
VARARGS(ShamrockSetWavelength)
//unsigned int WINAPI ShamrockGetWavelength(int device, float *wavelength);
VARARGS(ShamrockGetWavelength)
//unsigned int WINAPI ShamrockGotoZeroOrder(int device);
//unsigned int WINAPI ShamrockAtZeroOrder(int device, int *atZeroOrder);
//unsigned int WINAPI ShamrockGetWavelengthLimits(int device, int Grating, float *Min, float *Max);
//unsigned int WINAPI ShamrockWavelengthIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetAutoSlitWidth(int device, int index, float width);
//unsigned int WINAPI ShamrockGetAutoSlitWidth(int device, int index, float *width);
//unsigned int WINAPI ShamrockAutoSlitReset(int device, int index);
//unsigned int WINAPI ShamrockAutoSlitIsPresent(int device, int index, int *present);
//unsigned int WINAPI ShamrockSetAutoSlitCoefficients(int device, int index, int x1, int y1, int x2, int y2);
//unsigned int WINAPI ShamrockGetAutoSlitCoefficients(int device, int index, int &x1, int &y1, int &x2, int &y2);
//unsigned int WINAPI ShamrockSetShutter(int device, int mode);
//unsigned int WINAPI ShamrockGetShutter(int device, int *mode);
//unsigned int WINAPI ShamrockIsModePossible(int device, int mode, int *possible);
//unsigned int WINAPI ShamrockShutterIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetFilter(int device, int filter);
//unsigned int WINAPI ShamrockGetFilter(int device, int *filter);
//unsigned int WINAPI ShamrockGetFilterInfo(int device, int Filter, char* Info);
//unsigned int WINAPI ShamrockSetFilterInfo(int device, int Filter, char* Info);
//unsigned int WINAPI ShamrockFilterReset(int device);
//unsigned int WINAPI ShamrockFilterIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetFlipperMirror(int device, int flipper, int port);
//unsigned int WINAPI ShamrockGetFlipperMirror(int device, int flipper, int * port);
//unsigned int WINAPI ShamrockFlipperMirrorReset(int device, int flipper);
//unsigned int WINAPI ShamrockFlipperMirrorIsPresent(int device, int flipper, int *present);
//unsigned int WINAPI ShamrockGetCCDLimits(int device, int port, float *Low, float *High);
//unsigned int WINAPI ShamrockSetAccessory(int device, int Accessory, int State);
//unsigned int WINAPI ShamrockGetAccessoryState(int device, int Accessory, int *state);
//unsigned int WINAPI ShamrockAccessoryIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetFocusMirror(int device, int focus);
//unsigned int WINAPI ShamrockGetFocusMirror(int device, int *focus);
//unsigned int WINAPI ShamrockGetFocusMirrorMaxSteps(int device, int *steps);
//unsigned int WINAPI ShamrockFocusMirrorReset(int device);
//unsigned int WINAPI ShamrockFocusMirrorIsPresent(int device, int *present);
//unsigned int WINAPI ShamrockSetPixelWidth(int device, float Width);
VARARGS(ShamrockSetPixelWidth)
//unsigned int WINAPI ShamrockSetNumberPixels(int device, int NumberPixels);
VARARGS(ShamrockSetNumberPixels)
//unsigned int WINAPI ShamrockGetPixelWidth(int device, float* Width);
VARARGS(ShamrockGetPixelWidth)
//unsigned int WINAPI ShamrockGetNumberPixels(int device, int* NumberPixels);
VARARGS(ShamrockGetNumberPixels)
//unsigned int WINAPI ShamrockGetCalibration(int device, float* CalibrationValues, int NumberPixels);
INT_ARRAY_AND_SIZE(ShamrockGetCalibration)
//unsigned int WINAPI ShamrockGetPixelCalibrationCoefficients(int device, float* A, float* B, float* C, float* D);


