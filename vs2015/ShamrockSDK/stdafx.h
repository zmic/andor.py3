// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

#include <utility>
#include <map>
#include <vector>

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif
#include <python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
//#define PY_ARRAY_UNIQUE_SYMBOL SOME_NAME
#include <numpy\arrayobject.h>
#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG 1
#undef _DEBUG_WAS_DEFINED
#endif

#include <ShamrockCIF.h>

#define MODULE_NAME ShamrockSDK
#define MODULE_NAME_STRING "ShamrockSDK"


// TODO: reference additional headers your program requires here
