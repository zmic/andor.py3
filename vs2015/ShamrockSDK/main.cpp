#include "stdafx.h"

extern std::vector<PyMethodDef> v_method_defs;

#if PY_MAJOR_VERSION == 3

int init_functions()

static struct PyModuleDef the_module = {
	PyModuleDef_HEAD_INIT,
	MODULE_NAME_STRING,   /* name of module */
	0, /* module documentation, may be NULL */
	-1,       /* size of per-interpreter state of the module,
			  or -1 if the module keeps state in global variables. */
	0
};

PyMODINIT_FUNC
PyInit_AndorSDK(void)
{
#ifdef _DEBUG
	PyErr_WarnEx(PyExc_RuntimeWarning, "This is the debug build of AndorSDK.pyd", 0);
#endif
	init_functions();
	PyMethodDef sentinel = { 0,0,0,0 };
	v_method_defs.push_back(sentinel);
	the_module.m_methods = &v_method_defs[0];
	PyObject *m = PyModule_Create(&the_module);
	init_structs(m);
	return m;
}

#else
void init_functions();

PyMODINIT_FUNC
initShamrockSDK(void)
{
#ifdef _DEBUG
	PyErr_Warn(PyExc_RuntimeWarning, "this is the debug build of "MODULE_NAME_STRING".pyd");
#endif
	init_functions();
	PyObject *m;
	PyMethodDef sentinel = { 0,0,0,0 };
	v_method_defs.push_back(sentinel);
	m = Py_InitModule("ShamrockSDK", &v_method_defs[0]);
}

#endif
