import sys, os, time
import numpy as np

from andor_sdk import detect_platform
    
if detect_platform() == 'amd64':
    os.environ['path'] += r";C:\Program Files\Andor SOLIS\Drivers\Shamrock64"    
else:
    os.environ['path'] += r";C:\Program Files\Andor SOLIS\Drivers\Shamrock"
 
import ShamrockSDK as sham

class __proxyclass(object):
    def __init__(self):
        for k, v in sham.__dict__.items():
            if 0 == k.find("SHAMROCK_"):
                setattr(self, k, v)
    def __getattr__(self, fname):
        print "calling s."+fname,
        f = getattr(sham, fname)
        def dummy(*args):
            result = f(*args)
            if type(result) == tuple:
                error, result = result[0], result[1:]
                if len(result) == 1:
                    result = result[0]
                elif len(result) == 0:
                    result = None
            else:
                error, result = result, None
            print "=>", error
            if error != 20202:
                raise RuntimeError("Error %d"%error)
            return result
        return dummy
    
        
s = __proxyclass()

    
    
    
    





