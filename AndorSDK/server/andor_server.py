import sys, socket, Queue, thread, time, os
import imp
from andor_sdk import a
from shamrock_sdk import s

class fake_a(object):
    def __init__(self):
        self.cooleron=False
    def __getattr__(self, f):
        print "calling dummy for", f
        def dummy(*args):
            print f, args
        return dummy
    def IsCoolerOn(self):
        return self.cooleron
    def GetTemperatureRange(self):
        return -100,0 
    def SetTemperature(self, target):
        self.target = target
    def GetTemperature(self):
        return self.target if self.cooleron else 20
    def CoolerON(self):
        self.cooleron=True
    def CoolerOFF(self):
        self.cooleron=False
    
if os.environ['COMPUTERNAME'] == 'MEETPC0':      
    print "USING DUMMY SDK"
    a=fake_a()


class andor_server(object):
    def __init__(self, state_callback):
        self.state_callback = state_callback
        self.set_state("DISCONNECTED")
        self.Q = Queue.Queue() 
    def run(self):
        while 1:
            command, args = self.Q.get()            
            thread.start_new_thread( self.shell, (command, args) )
            self.Q.get()
    def post(self, command, args):
        self.Q.put( (command, args) )
            
    def shell(self, command, args):
        try:
            f = getattr(self, command)
            f(*args)
        except:
            raise
        finally:
            if command == 'run_script':
                self.set_state("READY")   
            self.Q.put('done')
            
    def set_state(self, state):      
        self.state = state    
        self.state_callback(state)
        
    def connect(self):
        self.set_state("CONNECTING")
        a.Initialize("")
        s.ShamrockInitialize("")
        mintemp, max_temp = a.GetTemperatureRange()
        print "mintemp, max_temp:",mintemp, max_temp
        self.set_state("READY")
        
    def cool(self, target_temperature):
        self.set_state("COOLING")
        if(not a.IsCoolerOn()):
            print "Switch on cooler"
            a.CoolerON()
        a.SetTemperature(target_temperature)
        self.set_state("SETTING TEMPERATURE")
        while 1:
            time.sleep(1)
            t = a.GetTemperature()
            print "GetTemperature:", a.GetTemperature()
            if abs(target_temperature - t) < 2:
                break
        print "TEMPERATURE REACHED"
        self.set_state("READY")
        
    def disconnect(self):
        self.set_state("WARMING UP")
        print "Switch off cooler"
        a.CoolerOFF()
        while 1:
            time.sleep(1)
            t = a.GetTemperature()
            print "GetTemperature:", a.GetTemperature()
            if t > 5:
                break
        print "shutting down..."
        a.ShutDown()
        self.set_state("DISCONNECTED")
        
    def run_script(self, path):
        self.set_state("RUNNING SCRIPT")
        print "#########################################################"
        print path        
        self.module = imp.load_source('imported_module', path)
        self.module.run()
        print "Script finished"
        
    
    '''                
    def listen(self):
        host = '' # Symbolic name meaning all available interfaces
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((host, self.listen_port))
        s.listen(0)        
        while 1:
            self.set_state("WAITING FOR SCRIPT")
            s2, addr = s.accept()
            print 'Connected by %r\n'%(addr,) 
            if addr[0] != '127.0.0.1':
                raise RuntimeError
            self.set_state("CONNECTED TO SCRIPT")
            thread.start_new_thread(self.socket_reader, (s2,) )
            self.Q.get()
                
    def socket_reader(self, socket):
        try:
            self.datagram_reader(socket)
        except:
            print "Unhandled exception in command handler"
            #dump = StringIO.StringIO()
            #traceback.print_exc(file=dump)            
            #self.stdlog(dump.getvalue())
            raise
        finally:
            self.Q.put("done")
            
    def datagram_reader(self, socket):
        buffer = ''
        while 1:
            while 1:
                data = self.socket.recv(2048)
                if not data: return
                buffer += data
                while 1:
                    pos = buffer.find('@')
                    if pos == -1: break
                    len_datagram = int( buffer[:pos] )
                    if len(buffer) < len_datagram + pos + 1:
                        break
                    next_datagram = buffer[pos+1:pos+1+len_datagram]
                    buffer = buffer[pos+1+len_datagram:]
            

    def send_datagram(self, data):
        self.lock.acquire()
        self.socket.send( '%d@'%len(data) )
        self.socket.send( data )
        self.lock.release()
    '''
           
    
        
        
        
    
    
        
         
    
    
    