import sys, os, time
import numpy as np
from andor_sdk import a
from shamrock_sdk import s

def run():
    # This function returns the size of the detector in pixels
    xpix, ypix = a.GetDetector()
    print("xpix, ypix:", xpix, ypix)
    
    caps = a.AndorCapabilities()
    caps.ulSize = 48
    a.GetCapabilities(caps)
    
    # 1 Single Scan 
    # 2 Accumulate 
    # 3 Kinetics 
    # 4 Fast Kinetics 
    # 5 Run till abort
    a.SetAcquisitionMode(1)
    
    # 0 Full Vertical Binning 
    # 1 Multi-Track 
    # 2 Random-Track 
    # 3 Single-Track 
    # 4 Image
    a.SetReadMode(0)
    
    ###################################################################################################
    
    # This function will set the exposure time to the nearest valid value not less than
    # the given value. The actual exposure time used is obtained by GetAcquisitionTimings.
    for et in np.arange(0.1,1,0.2):
        print "Exposure time", et
        a.SetExposureTime(et)        
        a.StartAcquisition()
        while 1:
            time.sleep(.05)
            status = a.GetStatus()
            if status == a.DRV_IDLE:
                break
        A = np.zeros( (xpix,), dtype = np.int32)            
        a.GetAcquiredData(A, xpix)
        x = range(len(A))


    

