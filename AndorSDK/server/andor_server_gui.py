import sys, time, random
from PyQt4.Qt import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QPushButton, QLabel, QLineEdit
from PyQt4.Qt import QIntValidator, QPlainTextEdit, QMainWindow
from PyQt4.QtCore import QThread, QEvent, pyqtSignal
from PyQt4 import QtGui
import andor_server as a

class Printer(object):
    class __box(object):
        def __init__(self, parent):
            self.parent = parent
        def __del__(self):
            self.parent.pop_prefix()
    def __del__(self):
        sys.stdout = self.stdout
    def __init__(self, write_function):
        self.write_function = write_function
        self.stdout = sys.stdout
        sys.stdout = self
        self.stderr = sys.stderr
        sys.stderr = self
        self.new_line = True
        self.__prefix = ''
        self.__prefix_stack = []
        self.show_time = True
    def __do_prefix(self):
        if self.show_time:
            self.write_function( time.strftime('[%H:%M:%S]     ') )
        self.write_function( self.__prefix )
    def __do_prefix_notime(self):
        self.write_function( self.__prefix )
    def box( self, prefix):
        self.push_prefix(prefix)
        return self.__box(self)
    def push_prefix(self, s):
        self.__prefix_stack.append(s)
        self.__prefix += s
    def pop_prefix(self):
        self.__prefix_stack.pop(-1)
        self.__prefix = ''.join(self.__prefix_stack)
    def skip(self):
        self.write_function('\n')
    def write(self, data):
        if not data:
            return
        if self.new_line:
            self.__do_prefix()
            self.new_line = False
        while data:
            pos = data.find('\n')
            if pos == -1:
                self.write_function(data)
                return
            self.write_function(data[:pos+1])
            data = data[pos+1:]
            if data:
                self.__do_prefix()
        self.new_line = True


class _Event(QEvent):
    EVENT_TYPE = QEvent.Type(QEvent.registerEventType())
    def __init__(self, i):
        #thread-safe
        QEvent.__init__(self, _Event.EVENT_TYPE)
        self.i = i
        
class Worker(QThread):
    def __init__(self, parent = None):    
        QThread.__init__(self, parent)
        self.parent = parent
    def state_callback(self, state):
        self.parent.state_signal.emit(state)
    def post(self, command, args):
        self.andor.post(command, args)
    def run(self):
        self.andor = a.andor_server(self.state_callback)      
        self.andor.run()        
        
          
class ButtonPanel(QWidget):
    Y_size = 30
    def __init__(self, list_of_button_labels, handler):
        super(QWidget, self).__init__()
        self.handler = handler
        self.buttons = {}
        for i, label in enumerate(list_of_button_labels):
            b = QPushButton(label, self)
            b.setMinimumWidth(145)
            b.move(0, i*self.Y_size)  
            b.clicked.connect(self.__handler)            
            self.buttons[label] = b
        self.setMinimumWidth(165)
    def disable(self, label):
        self.buttons[label].setEnabled(False)
    def enable(self, label):
        self.buttons[label].setEnabled(True)
    def __handler(self):
        sending_button = self.sender()
        self.handler(sending_button)
        
class MainWindow(QWidget):         
    def closeEvent(self, event):  
        event.ignore()
        '''
        if self.state == 'DISCONNECTED':
            event.accept()
        else:
            event.ignore()
        '''
        
    print_signal = pyqtSignal(str)        
    def print_function(self, data):
        self.print_signal.emit(data)
    def handle_print(self, data):
        self.text.moveCursor(QtGui.QTextCursor.End)
        self.text.insertPlainText(data)
        self.text.moveCursor(QtGui.QTextCursor.End)

    state_signal = pyqtSignal(str)        
    def set_state(self, new_state):
        self.state_signal.emit(new_state)
    def handle_state(self, new_state):
        self.state = new_state
        self.setWindowTitle('Andor Python Server - '+new_state + ' - ' + self.selected_script)     
        if new_state == 'DISCONNECTED':        
            self.button_panel.enable('Connect')
            self.button_panel.disable('Cool')            
            self.button_panel.disable('Disconnect')
            self.button_panel.disable('Run Script')
            self.button_panel.disable('Break')
        elif new_state == 'READY':        
            self.button_panel.disable('Connect')
            self.button_panel.enable('Cool')            
            self.button_panel.enable('Disconnect')
            self.button_panel.enable('Run Script')
            self.button_panel.disable('Break')
        elif new_state == 'COOLING' or new_state == 'WARMING UP':        
            self.button_panel.disable('Connect')
            self.button_panel.disable('Cool')            
            self.button_panel.disable('Disconnect')
            self.button_panel.disable('Run Script')
            self.button_panel.disable('Break')
        elif new_state == 'RUNNING SCRIPT':        
            self.button_panel.disable('Connect')
            self.button_panel.disable('Cool')            
            self.button_panel.disable('Disconnect')
            self.button_panel.disable('Run Script')
            self.button_panel.enable('Break')
            
    def __init__(self):
        super(MainWindow, self).__init__()
        self.selected_script = ''
        self.state = ''        
        self.print_signal.connect(self.handle_print)
        self.state_signal.connect(self.handle_state)
        
        self.setMinimumSize(400, 185)
        self.setWindowTitle('Andor Python Shell')        
        self.resize(800, 250)
        layout0 = QHBoxLayout(self)

        layout1 = QVBoxLayout()
        labels = ['Connect', 'Cool', 'Select Script', 'Run Script', 'Disconnect', 'Break', 'Tickle me']
        self.button_panel = ButtonPanel(labels, self.handleButton)
        layout1.addWidget(self.button_panel)
        
        lineedit = QLineEdit(self)
        lineedit.setMaximumWidth(45)
        layout1.addWidget(lineedit)
        layout0.addLayout(layout1)
        validator = QIntValidator()
        validator.setRange(-80,15)
        lineedit.setValidator(validator)
        lineedit.setText("0")
        lineedit.textChanged.connect(self.check_temperature_input)
        lineedit.textChanged.emit(lineedit.text())
        self.temperature_edit = lineedit
        
        self.text = QPlainTextEdit()
        self.text.setReadOnly(True)
        self.text.setMinimumWidth(165)
        layout0.addWidget(self.text)
        self.P = Printer(self.print_function)
        
        self.thread = Worker(self)
        self.thread.start()
        
        #self.connect(self, Qt.SIGNAL('triggered()'), self.closeEvent)   
        #self.connect(self, SIGNAL("hello_from__thread"), self.handle_event_from_thread)
        #self.connect(self.thread, SIGNAL("handle"), self.addImage)        
        #self.set_state("Disconnected")
        
    def handle_event_from_thread(self, value):
        print value1        
        
    def check_temperature_input(self, *args, **kwargs):
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        if state == QtGui.QValidator.Acceptable:
            color = '#c4df9b' # green
        elif state == QtGui.QValidator.Intermediate:
            color = '#fff79a' # yellow
        else:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)
    
    def handleButton(self, button):
        button_label = button.text()
        if button_label == 'Tickle me':
            print 'he' + 'he'*random.randint(0,10)
        elif button_label == 'Connect':
            self.thread.post("connect", ())
        elif button_label == 'Cool':
            self.thread.post("cool", (self.temperature_edit.text().toInt()[0],))
        elif button_label == 'Disconnect':
            self.thread.post("disconnect", ())
        elif button_label == 'Select Script':            
            fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '', 'Python script (*.py)')
            self.selected_script = str(fname)
            print "selected script", self.selected_script
            self.setWindowTitle('Andor Python Server - ' + self.state + ' - ' + self.selected_script)        
        elif button_label == 'Run Script':
            if self.selected_script:
                self.thread.post("run_script", (self.selected_script,))
            else:
                print "Select a script first!"
        elif button_label == 'Break':
            self.thread.post("break", ())
           
    def closeEvent(self, evnt):
        if self._want_to_close:
            super(MainWindow, self).closeEvent(evnt)
        else:
            evnt.ignore()
            self.setWindowState(QtCore.Qt.WindowMinimized)
            
if __name__ == '__main__':
    application = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(application.exec_())
    
    