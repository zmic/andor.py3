import sys, os, time
import numpy as np
from pymeasure.units.unit import NANOMETER, SECOND, DBM
from pymeasure.instruments.M2.tunable_laser import SolsTiS
from pymeasure.util.png import png
from pymeasure.util.csv import csv
from pymeasure.util.result_folder import now_folder

from andor_sdk import a
from shamrock_sdk import s

def run():

    #print s.ShamrockGetGrating(0)
    #print s.ShamrockGetNumberGratings(0)
    #print s.ShamrockGetWavelength(0)
    nbr_pixels = s.ShamrockGetNumberPixels(0)
    X = np.empty( (nbr_pixels,), dtype = np.float32)     
    s.ShamrockSetWavelength(0, 750)
    print "shamrock WL set to:", s.ShamrockGetWavelength(0)
    s.ShamrockGetCalibration(0, X, nbr_pixels)

    
    L = SolsTiS(name='SolsTiS laser', host='157.193.173.27', port=39999)    
    L.ping("Hello World!")

    # This function returns the size of the detector in pixels
    xpix, ypix = a.GetDetector()
    print "xpix, ypix:", xpix, ypix
    assert xpix == nbr_pixels
        
    # 1 Single Scan 
    # 2 Accumulate 
    # 3 Kinetics 
    # 4 Fast Kinetics 
    # 5 Run till abort
    a.SetAcquisitionMode(1)
    
    # 0 Full Vertical Binning 
    # 1 Multi-Track 
    # 2 Random-Track 
    # 3 Single-Track 
    # 4 Image
    a.SetReadMode(0)
    
    ###################################################################################################
     
    et = 1
    print "Exposure time", et
    a.SetExposureTime(et)        
    
    folder = now_folder(r"c:\Users\meetkamer\Desktop\Haolan") 
    print "saving to", folder
    #for wl in np.arange(785.0, 786.0, 0.001):  
    for wl in np.arange(785.0, 786.0, 0.5):   
        print "set to", wl
        actual_wl = L.set_tuning(wl*NANOMETER)
        print "actual wavelength", actual_wl.get_value()
        a.StartAcquisition()
        while 1:
            time.sleep(.05)
            status = a.GetStatus()
            if status == a.DRV_IDLE:
                break
        Y = np.zeros( (nbr_pixels,), dtype = np.int32)            
        a.GetAcquiredData(Y, nbr_pixels)
        plot = png(folder+os.sep+"WL_%f.png"%wl)
        plot.plot(X, Y)
        csv(folder+os.sep+"WL_%f.csv"%wl, X, Y)
        
        
    

