template<typename ptr_type, NPY_TYPES expected_type>
bool check_numpy_array(const char* name, PyArrayObject *obj, ptr_type& data)
{
	if (!PyArray_Check(obj))
	{
		set_python_error(PyExc_ValueError, "%s should be numpy array", name);
		return false;
	}
	if (PyArray_TYPE(obj) != expected_type)
	{
		set_python_error(PyExc_ValueError, "%s is numpy array of wrong type", name);
		return false;
	}
	if (PyArray_NDIM(obj) != 1)
	{
		set_python_error(PyExc_ValueError, "%s should be 1-dimensional", name);
		return false;
	}
	data = (ptr_type)PyArray_DATA(obj);
	return true;
}


PyObject* pyint(int v);
PyObject* pyint(long v);
PyObject* pyint(unsigned int v);
PyObject* pyint(unsigned long v);

