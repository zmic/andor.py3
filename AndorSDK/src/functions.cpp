#include "include.h"
#include "util.h"
#include "structs.h"

#if PY_MAJOR_VERSION == 3
int init_functions()
{
    import_array();
	return 0;
}
#else
void init_functions()
{
	import_array();
}
#endif


#if PY_MAJOR_VERSION == 3
PyObject* pyint(int v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(long v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(unsigned int v)
{
	return PyLong_FromLong(v);
}
PyObject* pyint(unsigned long v)
{
	return PyLong_FromLong(v);
}
#else
PyObject* pyint(int v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(long v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(unsigned int v)
{
	return PyInt_FromLong(v);
}
PyObject* pyint(unsigned long v)
{
	return PyInt_FromLong(v);
}
#endif

bool is_error(int e);
void set_python_error(PyObject* exc_type, char * format, ...);

///////////////////////////////////////////////////////////////////////////////////////////

std::vector<PyMethodDef> v_method_defs;

///////////////////////////////////////////////////////////////////////////////////////////
#define CHECK_ARGS(...)\
do {\
if (!PyArg_ParseTuple(args, __VA_ARGS__))\
{\
	goto error;\
}\
} while (0)



///////////////////////////////////////////////////////////////////////////////////////////
template<typename POINTER2FUNCTION>
PyObject* py_wrapper_simple(POINTER2FUNCTION pf);

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(void))
{
	unsigned int error = pf();
	return pyint(error);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(int*))
{
	int value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(long*))
{
	long value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(unsigned long*))
{
	unsigned long value = 0;
	int error = (pf(&value));
	return Py_BuildValue("ii", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*))
{
	float value = 0;
	int error = (pf(&value));
	return Py_BuildValue("if", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(double*))
{
	double value = 0;
	int error = (pf(&value));
	return Py_BuildValue("id", error, value);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(int*, int*))
{
	int i1 = 0;
	int i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(long*, long*))
{
	long i1 = 0;
	long i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(unsigned long*, unsigned long*))
{
	unsigned long i1 = 0;
	unsigned long i2 = 0;
	int error = (pf(&i1, &i2));
	return Py_BuildValue("iii", error, i1, i2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	int error = (pf(&f1, &f2));
	return Py_BuildValue("iff", error, f1, f2);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	float f3 = 0;
	int error = (pf(&f1, &f2, &f3));
	return Py_BuildValue("ifff", error, f1, f2, f3);
}

template<>
PyObject* py_wrapper_simple(unsigned int (WINAPI *pf)(float*, float*, float*, float*))
{
	float f1 = 0;
	float f2 = 0;
	float f3 = 0;
	float f4 = 0;
	int error = (pf(&f1, &f2, &f3, &f4));
	return Py_BuildValue("iffff", error, f1, f2, f3, f4);
}

int push_NOARGS(const char* name, PyObject* (*pf)(PyObject*))
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_NOARGS, NULL };
	v_method_defs.push_back(a);
	return 0;
}

#define PUSH_NOARGS(name)\
namespace PYNS_##name\
{\
    int i = push_NOARGS( #name, py__##name );\
}

#define NOARGS(api_function)\
PyObject* py__##api_function(PyObject* self);\
PUSH_NOARGS(api_function)\
PyObject* py__##api_function(PyObject* self)\
{\
	return py_wrapper_simple(api_function);\
}\

///////////////////////////////////////////////////////////////////////////////////////////
template<typename POINTER2FUNCTION>
PyObject* py_wrapper_simple(PyObject* args, POINTER2FUNCTION pf);

int push_VARARGS(const char* name, PyObject* (*pf)(PyObject*, PyObject*))
{
	PyMethodDef a = { name, (PyCFunction)pf, METH_VARARGS, NULL };
	v_method_defs.push_back(a);
	return 0;
}

#define PUSH_VARARGS(name)\
namespace PYNS_##name\
{\
    int i = push_VARARGS( #name, py__##name );\
}

#define VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return py_wrapper_simple(args, api_function);\
}\

///////////////////////////////////////////////////////////////////////////////////////////
//
//  specializations of py_wrapper_simple 
//

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int))
{
	int i;
	CHECK_ARGS("i", &i);
	int error = (pf(i));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(char))
{
	char c;
	CHECK_ARGS("b", &c);
	int error = (pf(c));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(float))
{
	float f;
	CHECK_ARGS("f", &f);
	int error = (pf(f));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(double))
{
	double d;
	CHECK_ARGS("d", &d);
	int error = (pf(d));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(char*))
{
	char* s;
	CHECK_ARGS("s", &s);
	int error = (pf(s));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int))
{
	int i1;
	int i2;
	CHECK_ARGS("ii", &i1, &i2);
	int error = (pf(i1, i2));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int))
{
	int i1;
	int i2;
	int i3;
	CHECK_ARGS("iii", &i1, &i2, &i3);
	int error = (pf(i1, i2, i3));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int))
{
	int i1, i2, i3, i4, i5;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5);
	int error = (pf(i1, i2, i3, i4, i5));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int, int))
{
	int i1, i2, i3, i4, i5, i6;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5, &i6);
	int error = (pf(i1, i2, i3, i4, i5, i6));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int, int, int, int))
{
	int i1, i2, i3, i4, i5, i6, i7;
	CHECK_ARGS("iii", &i1, &i2, &i3, &i4, &i5, &i6, &i7);
	int error = (pf(i1, i2, i3, i4, i5, i6, i7));
	return pyint(error);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int*))
{
	int i;
	int i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, int, int, int*, int*))
{
	int i0, i1, i2;
	int io0 = 0, io1 = 0;
	CHECK_ARGS("iii", &i0, &i1, &i2);
	int error = (pf(i0, i1, i2, &io0, &io1));
	return Py_BuildValue("iii", error, io0, io1);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(long, long*))
{
	long i;
	long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, long*))
{
	int i;
	long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}


template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(unsigned long, unsigned long*))
{
	unsigned long i;
	unsigned long i_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &i_out));
	return Py_BuildValue("ii", error, i_out);
error:
	return 0;
}

template<> PyObject* py_wrapper_simple(PyObject* args, unsigned int (WINAPI*pf)(int, float*))
{
	int i;
	float f_out = 0;
	CHECK_ARGS("i", &i);
	int error = (pf(i, &f_out));
	return Py_BuildValue("if", error, f_out);
error:
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////

template<typename ptr_type, NPY_TYPES expected_type>
PyObject* __py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(ptr_type, unsigned long))
{
	PyArrayObject* array_object;
	unsigned long size;
	ptr_type data;
	CHECK_ARGS("Oi", &array_object, &size);
	if (!check_numpy_array<ptr_type, expected_type>("First argument", array_object, data))
	{
		goto error;
	}
	int error = (pf(data,size));
	return pyint(error);
error:
	return 0;
}

template<typename ptr_type>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(ptr_type, unsigned long));

template<>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(long*, unsigned long))
{
	return __py_wrapper_array_and_size<long*, NPY_INT32>(args, pf);
}
template<>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(unsigned short*, unsigned long))
{
	return __py_wrapper_array_and_size<unsigned short*, NPY_UINT16>(args, pf);
}
template<>
PyObject* py_wrapper_array_and_size(PyObject* args, unsigned int (WINAPI*pf)(float*, unsigned long))
{
	return __py_wrapper_array_and_size<float*, NPY_FLOAT32>(args, pf);
}


#define ARRAY_AND_SIZE(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return py_wrapper_array_and_size(args, api_function);\
}\


///////////////////////////////////////////////////////////////////////////////////////////

template<typename size_type, typename ptr_type, NPY_TYPES expected_type>
PyObject* __py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, ptr_type))
{
	size_type size;
	PyArrayObject* array_object;
	ptr_type data;
	CHECK_ARGS("iO", &size, &array_object);
	if (!check_numpy_array<ptr_type, expected_type>("Second argument", array_object, data))
	{
		goto error;
	}
	int error = (pf(size, data));
	return pyint(error);
error:
	return 0;
}

template<typename size_type, typename ptr_type>
PyObject* py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, ptr_type));

template<typename size_type>
PyObject* py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, int*))
{
	return __py_wrapper_size_and_array<size_type, int*, NPY_INT32>(args, pf);
}
template<typename size_type>
PyObject* py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, unsigned long*))
{
	return __py_wrapper_size_and_array<size_type, unsigned long*, NPY_UINT32>(args, pf);
}
template<typename size_type>
PyObject* py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, unsigned short*))
{
	return __py_wrapper_size_and_array<size_type, unsigned short*, NPY_UINT16>(args, pf);
}
template<typename size_type>
PyObject* py_wrapper_size_and_array(PyObject* args, unsigned int (WINAPI*pf)(size_type, float*))
{
	return __py_wrapper_size_and_array<size_type, float*, NPY_FLOAT32>(args, pf);
}

#define SIZE_AND_ARRAY(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return py_wrapper_size_and_array(args, api_function);\
}\

///////////////////////////////////////////////////////////////////////////////////////////


template<typename wrapped_type>
PyObject* __py_wrapped_type(PyObject* args, unsigned int (WINAPI*pf)(typename wrapped_type::_T*))
{
	wrapped_type* wrapped_object;
	CHECK_ARGS("O!", &wrapped_type::typeobject, &wrapped_object );
	int error = (pf(&wrapped_object->_cargo));
	return pyint(error);
error:
	return 0;
}

#define WRAPPED_TYPE(api_function, struct_type)\
PyObject* py__##api_function(PyObject* self, PyObject* args);\
PUSH_VARARGS(api_function)\
PyObject* py__##api_function(PyObject* self, PyObject* args)\
{\
	return __py_wrapped_type<py_##struct_type>(args, api_function);\
}\


///////////////////////////////////////////////////////////////////////////////////////////


//  unsigned int WINAPI (\w+)\(int \* \w+\);


//unsigned int WINAPI AbortAcquisition(void);
NOARGS(AbortAcquisition);

//unsigned int WINAPI CancelWait(void);
NOARGS(CancelWait);

//unsigned int WINAPI CoolerOFF(void);
NOARGS(CoolerOFF);

//unsigned int WINAPI CoolerON(void);
NOARGS(CoolerON);

//unsigned int WINAPI DemosaicImage(WORD * grey, WORD * red, WORD * green, WORD * blue, ColorDemosaicInfo * info);
unsigned int WINAPI DemosaicImage(WORD * grey, WORD * red, WORD * green, WORD * blue, ColorDemosaicInfo * info);

//unsigned int WINAPI EnableKeepCleans(int iMode);
VARARGS(EnableKeepCleans);

//unsigned int WINAPI EnableSensorCompensation(int iMode);
VARARGS(EnableSensorCompensation);

//unsigned int WINAPI SetIRIGModulation(char mode);
VARARGS(SetIRIGModulation);

//unsigned int WINAPI FreeInternalMemory(void);
NOARGS(FreeInternalMemory);

//unsigned int WINAPI GetAcquiredData(at_32 * arr, unsigned long size);
ARRAY_AND_SIZE(GetAcquiredData);

//unsigned int WINAPI GetAcquiredData16(WORD * arr, unsigned long size);
ARRAY_AND_SIZE(GetAcquiredData16);

//unsigned int WINAPI GetAcquiredFloatData(float * arr, unsigned long size);
ARRAY_AND_SIZE(GetAcquiredFloatData);

//unsigned int WINAPI GetAcquisitionProgress(long * acc, long * series);
NOARGS(GetAcquisitionProgress);

//unsigned int WINAPI GetAcquisitionTimings(float * exposure, float * accumulate, float * kinetic);
NOARGS(GetAcquisitionTimings);

//unsigned int WINAPI GetAdjustedRingExposureTimes(int inumTimes, float * fptimes);
SIZE_AND_ARRAY(GetAdjustedRingExposureTimes);

//unsigned int WINAPI GetAllDMAData(at_32 * arr, unsigned long size);
ARRAY_AND_SIZE(GetAllDMAData);

//unsigned int WINAPI GetAmpDesc(int index, char * name, int length);
unsigned int WINAPI GetAmpDesc(int index, char * name, int length);

unsigned int WINAPI GetAmpMaxSpeed(int index, float * speed);
VARARGS(GetAmpMaxSpeed);

//unsigned int WINAPI GetAvailableCameras(long * totalCameras);
NOARGS(GetAvailableCameras);

//unsigned int WINAPI GetBackground(at_32 * arr, unsigned long size);
ARRAY_AND_SIZE(GetBackground);

//unsigned int WINAPI GetBaselineClamp(int * state);
NOARGS(GetBaselineClamp);

//unsigned int WINAPI GetBitDepth(int channel, int * depth);
VARARGS(GetBitDepth);

//unsigned int WINAPI GetCameraEventStatus(DWORD * camStatus);
NOARGS(GetCameraEventStatus);

//unsigned int WINAPI GetCameraHandle(long cameraIndex, long * cameraHandle);
VARARGS(GetCameraHandle);

//unsigned int WINAPI GetCameraInformation(int index, long * information);
VARARGS(GetCameraInformation);

//unsigned int WINAPI GetCameraSerialNumber(int * number);
NOARGS(GetCameraSerialNumber);

//unsigned int WINAPI GetCapabilities(AndorCapabilities * caps);
WRAPPED_TYPE(GetCapabilities, AndorCapabilities);

//unsigned int WINAPI GetControllerCardModel(char * controllerCardModel);
unsigned int WINAPI GetControllerCardModel(char * controllerCardModel);

//unsigned int WINAPI GetCountConvertWavelengthRange(float * minval, float * maxval);
NOARGS(GetCountConvertWavelengthRange);

//unsigned int WINAPI GetCurrentCamera(long * cameraHandle);
NOARGS(GetCurrentCamera);

//unsigned int WINAPI GetCYMGShift(int * iXshift, int * iYShift);
NOARGS(GetCYMGShift);

//unsigned int WINAPI GetDDGExternalOutputEnabled(at_u32 uiIndex, at_u32 * puiEnabled);
VARARGS(GetDDGExternalOutputEnabled);

//unsigned int WINAPI GetDDGExternalOutputPolarity(at_u32 uiIndex, at_u32 * puiPolarity);
unsigned int WINAPI GetDDGExternalOutputPolarity(at_u32 uiIndex, at_u32 * puiPolarity);

//unsigned int WINAPI GetDDGExternalOutputStepEnabled(at_u32 uiIndex, at_u32 * puiEnabled);
unsigned int WINAPI GetDDGExternalOutputStepEnabled(at_u32 uiIndex, at_u32 * puiEnabled);

//unsigned int WINAPI GetDDGExternalOutputTime(at_u32 uiIndex, at_u64 * puiDelay, at_u64 * puiWidth);
unsigned int WINAPI GetDDGExternalOutputTime(at_u32 uiIndex, at_u64 * puiDelay, at_u64 * puiWidth);

//unsigned int WINAPI GetDDGTTLGateWidth(at_u64 opticalWidth, at_u64 * ttlWidth);
unsigned int WINAPI GetDDGTTLGateWidth(at_u64 opticalWidth, at_u64 * ttlWidth);

//unsigned int WINAPI GetDDGGateTime(at_u64 * puiDelay, at_u64 * puiWidth);
unsigned int WINAPI GetDDGGateTime(at_u64 * puiDelay, at_u64 * puiWidth);

//unsigned int WINAPI GetDDGInsertionDelay(int * piState);
NOARGS(GetDDGInsertionDelay);
//unsigned int WINAPI GetDDGIntelligate(int * piState);
NOARGS(GetDDGIntelligate);
//unsigned int WINAPI GetDDGIOC(int * state);
NOARGS(GetDDGIOC);
//unsigned int WINAPI GetDDGIOCFrequency(double * frequency);
NOARGS(GetDDGIOCFrequency);
//unsigned int WINAPI GetDDGIOCNumber(unsigned long * numberPulses);
NOARGS(GetDDGIOCNumber);
unsigned int WINAPI GetDDGIOCNumberRequested(at_u32 * pulses);
unsigned int WINAPI GetDDGIOCPeriod(at_u64 * period);
NOARGS(GetDDGIOCPulses);
unsigned int WINAPI GetDDGIOCTrigger(at_u32 * trigger);
unsigned int WINAPI GetDDGOpticalWidthEnabled(at_u32 * puiEnabled);

// DDG Lite functions
unsigned int WINAPI GetDDGLiteGlobalControlByte(unsigned char * control);
unsigned int WINAPI GetDDGLiteControlByte(AT_DDGLiteChannelId channel, unsigned char * control);
unsigned int WINAPI GetDDGLiteInitialDelay(AT_DDGLiteChannelId channel, float * fDelay);
unsigned int WINAPI GetDDGLitePulseWidth(AT_DDGLiteChannelId channel, float * fWidth);
unsigned int WINAPI GetDDGLiteInterPulseDelay(AT_DDGLiteChannelId channel, float * fDelay);
unsigned int WINAPI GetDDGLitePulsesPerExposure(AT_DDGLiteChannelId channel, at_u32 * ui32Pulses);

unsigned int WINAPI GetDDGPulse(double wid, double resolution, double * Delay, double * Width);
unsigned int WINAPI GetDDGStepCoefficients(at_u32 mode, double * p1, double * p2);
unsigned int WINAPI GetDDGWidthStepCoefficients(at_u32 mode, double * p1, double * p2);
unsigned int WINAPI GetDDGStepMode(at_u32 * mode);
unsigned int WINAPI GetDDGWidthStepMode(at_u32 * mode);
NOARGS(GetDetector);
unsigned int WINAPI GetDICameraInfo(void * info);
NOARGS(GetEMAdvanced);
NOARGS(GetEMCCDGain);
NOARGS(GetEMGainRange);
unsigned int WINAPI GetExternalTriggerTermination(at_u32 * puiTermination);
unsigned int WINAPI GetFastestRecommendedVSSpeed(int * index, float * speed);
NOARGS(GetFIFOUsage);
NOARGS(GetFilterMode);
NOARGS(GetFKExposureTime);
VARARGS(GetFKVShiftSpeed);
unsigned int WINAPI GetFKVShiftSpeedF(int index, float * speed);
NOARGS(GetFrontEndStatus);
NOARGS(GetGateMode);
unsigned int WINAPI GetHardwareVersion(unsigned int * PCB, unsigned int * Decode, unsigned int * dummy1, unsigned int * dummy2, unsigned int * CameraFirmwareVersion, unsigned int * CameraFirmwareBuild);
unsigned int WINAPI GetHeadModel(char * name);
VARARGS(GetHorizontalSpeed);
unsigned int WINAPI GetHSSpeed(int channel, int typ, int index, float * speed);
NOARGS(GetHVflag);
VARARGS(GetID);
NOARGS(GetImageFlip);
NOARGS(GetImageRotate);
unsigned int WINAPI GetImages(long first, long last, at_32 * arr, unsigned long size, long * validfirst, long * validlast);
unsigned int WINAPI GetImages16(long first, long last, WORD * arr, unsigned long size, long * validfirst, long * validlast);
NOARGS(GetImagesPerDMA);
NOARGS(GetIRQ);
NOARGS(GetKeepCleanTime);
unsigned int WINAPI GetMaximumBinning(int ReadMode, int HorzVert, int * MaxBinning);
NOARGS(GetMaximumExposure);
NOARGS(GetMaximumNumberRingExposureTimes);
NOARGS(GetMCPGain);
NOARGS(GetMCPGainRange);
unsigned int WINAPI GetMCPGainTable(int iNum, int * piGain, float * pfPhotoepc);
NOARGS(GetMCPVoltage);
NOARGS(GetMinimumImageLength);
NOARGS(GetMinimumNumberInSeries);
unsigned int WINAPI GetMostRecentColorImage16(unsigned long size, int algorithm, WORD * red, WORD * green, WORD * blue);

//unsigned int WINAPI GetMostRecentImage(at_32 * arr, unsigned long size);
ARRAY_AND_SIZE(GetMostRecentImage);

//unsigned int WINAPI GetMostRecentImage16(WORD * arr, unsigned long size);
ARRAY_AND_SIZE(GetMostRecentImage16);

unsigned int WINAPI GetMSTimingsData(SYSTEMTIME * TimeOfStart, float * pfDifferences, int inoOfImages);
unsigned int WINAPI GetMetaDataInfo(SYSTEMTIME * TimeOfStart, float * pfTimeFromStart, unsigned int index);
NOARGS(GetMSTimingsEnabled);
unsigned int WINAPI GetNewData(at_32 * arr, unsigned long size);
unsigned int WINAPI GetNewData16(WORD * arr, unsigned long size);
unsigned int WINAPI GetNewData8(unsigned char * arr, unsigned long size);
unsigned int WINAPI GetNewFloatData(float * arr, unsigned long size);
NOARGS(GetNumberADChannels);
NOARGS(GetNumberAmp);
unsigned int WINAPI GetNumberAvailableImages(at_32 * first, at_32 * last);
unsigned int WINAPI GetNumberDDGExternalOutputs(at_u32 * puiCount);
NOARGS(GetNumberDevices);
NOARGS(GetNumberFKVShiftSpeeds);
NOARGS(GetNumberHorizontalSpeeds);
unsigned int WINAPI GetNumberHSSpeeds(int channel, int typ, int * speeds);
unsigned int WINAPI GetNumberMissedExternalTriggers(unsigned int first, unsigned int last, WORD * arr, unsigned int size);
unsigned int WINAPI GetIRIGData(unsigned char * _uc_irigData, unsigned int _ui_index);
unsigned int WINAPI GetNumberNewImages(long * first, long * last);
unsigned int WINAPI GetNumberPhotonCountingDivisions(at_u32 * noOfDivisions);
NOARGS(GetNumberPreAmpGains);
NOARGS(GetNumberRingExposureTimes);
NOARGS(GetNumberIO);
NOARGS(GetNumberVerticalSpeeds);
NOARGS(GetNumberVSAmplitudes);
NOARGS(GetNumberVSSpeeds);

//unsigned int WINAPI GetOldestImage(at_32 * arr, unsigned long size);
ARRAY_AND_SIZE(GetOldestImage);

//unsigned int WINAPI GetOldestImage16(WORD * arr, unsigned long size);
ARRAY_AND_SIZE(GetOldestImage16);

NOARGS(GetPhosphorStatus);

//unsigned int WINAPI GetPhysicalDMAAddress(unsigned long * Address1, unsigned long * Address2);
NOARGS(GetPhysicalDMAAddress);

//unsigned int WINAPI GetPixelSize(float * xSize, float * ySize);
NOARGS(GetPixelSize);

unsigned int WINAPI GetPreAmpGain(int index, float * gain);
VARARGS(GetPreAmpGain);

unsigned int WINAPI GetPreAmpGainText(int index, char * name, int length);
unsigned int WINAPI GetDualExposureTimes(float * exposure1, float * exposure2);
unsigned int WINAPI GetQE(char * sensor, float wavelength, unsigned int mode, float * QE);
NOARGS(GetReadOutTime);
NOARGS(GetRegisterDump);
unsigned int WINAPI GetRelativeImageTimes(unsigned int first, unsigned int last, at_u64 * arr, unsigned int size);

//unsigned int WINAPI GetRingExposureRange(float * fpMin, float * fpMax);
NOARGS(GetRingExposureRange);

NOARGS(GetSDK3Handle);
unsigned int WINAPI GetSensitivity(int channel, int horzShift, int amplifier, int pa, float * sensitivity);
NOARGS(GetShutterMinTimes);
NOARGS(GetSizeOfCircularBuffer);
unsigned int WINAPI GetSlotBusDeviceFunction(DWORD * dwslot, DWORD * dwBus, DWORD * dwDevice, DWORD * dwFunction);
unsigned int WINAPI GetSoftwareVersion(unsigned int * eprom, unsigned int * coffile, unsigned int * vxdrev, unsigned int * vxdver, unsigned int * dllrev, unsigned int * dllver);
NOARGS(GetSpoolProgress);
NOARGS(GetStartUpTime);
NOARGS(GetStatus);
NOARGS(GetTECStatus);
NOARGS(GetTemperature);
NOARGS(GetTemperatureF);
NOARGS(GetTemperatureRange);
NOARGS(GetTemperaturePrecision);

//unsigned int WINAPI GetTemperatureStatus(float * SensorTemp, float * TargetTemp, float * AmbientTemp, float * CoolerVolts);
NOARGS(GetTemperatureStatus);

NOARGS(GetTotalNumberImagesAcquired);
VARARGS(GetIODirection);
VARARGS(GetIOLevel);
unsigned int WINAPI GetUSBDeviceDetails(WORD * VendorID, WORD * ProductID, WORD * FirmwareVersion, WORD * SpecificationNumber);
unsigned int WINAPI GetVersionInfo(AT_VersionInfoId arr, char * szVersionInfo, at_u32 ui32BufferLen);
VARARGS(GetVerticalSpeed);
unsigned int WINAPI GetVirtualDMAAddress(void ** Address1, void ** Address2);
unsigned int WINAPI GetVSAmplitudeString(int index, char * text);
unsigned int WINAPI GetVSAmplitudeFromString(char * text, int * index);
VARARGS(GetVSAmplitudeValue);
unsigned int WINAPI GetVSSpeed(int index, float * speed);
unsigned int WINAPI GPIBReceive(int id, short address, char * text, int size);
unsigned int WINAPI GPIBSend(int id, short address, char * text);
unsigned int WINAPI I2CBurstRead(BYTE i2cAddress, long nBytes, BYTE * data);
unsigned int WINAPI I2CBurstWrite(BYTE i2cAddress, long nBytes, BYTE * data);
unsigned int WINAPI I2CRead(BYTE deviceID, BYTE intAddress, BYTE * pdata);
NOARGS(I2CReset);
unsigned int WINAPI I2CWrite(BYTE deviceID, BYTE intAddress, BYTE data);
NOARGS(IdAndorDll);
VARARGS(InAuxPort);

//unsigned int WINAPI Initialize(char * dir);
VARARGS(Initialize);

unsigned int WINAPI InitializeDevice(char * dir);
VARARGS(IsAmplifierAvailable);
NOARGS(IsCoolerOn);
VARARGS(IsCountConvertModeAvailable);
NOARGS(IsInternalMechanicalShutter);
unsigned int WINAPI IsPreAmpGainAvailable(int channel, int amplifier, int index, int pa, int * status);
VARARGS(IsReadoutFlippedByAmplifier);
VARARGS(IsTriggerModeAvailable);
unsigned int WINAPI Merge(const at_32 * arr, long nOrder, long nPoint, long nPixel, float * coeff, long fit, long hbin, at_32 * output, float * start, float * step_Renamed);
VARARGS(OutAuxPort);
NOARGS(PrepareAcquisition);
unsigned int WINAPI SaveAsBmp(const char * path, const char * palette, long ymin, long ymax);
unsigned int WINAPI SaveAsCommentedSif(char * path, char * comment);
unsigned int WINAPI SaveAsEDF(char * szPath, int iMode);
unsigned int WINAPI SaveAsFITS(char * szFileTitle, int typ);
unsigned int WINAPI SaveAsRaw(char * szFileTitle, int typ);
unsigned int WINAPI SaveAsSif(char * path);
unsigned int WINAPI SaveAsSPC(char * path);
unsigned int WINAPI SaveAsTiff(char * path, char * palette, int position, int typ);
unsigned int WINAPI SaveAsTiffEx(char * path, char * palette, int position, int typ, int mode);
unsigned int WINAPI SaveEEPROMToFile(char * cFileName);
unsigned int WINAPI SaveToClipBoard(char * palette);
VARARGS(SelectDevice);
NOARGS(SendSoftwareTrigger);
VARARGS(SetAccumulationCycleTime);
unsigned int WINAPI SetAcqStatusEvent(HANDLE statusEvent);
VARARGS(SetAcquisitionMode);
VARARGS(SetSensorPortMode);
VARARGS(SelectSensorPort);
VARARGS(SetAcquisitionType);
VARARGS(SetADChannel);
VARARGS(SetAdvancedTriggerModeState);
unsigned int WINAPI SetBackground(at_32 * arr, unsigned long size);
VARARGS(SetBaselineClamp);
VARARGS(SetBaselineOffset);
VARARGS(SetCameraLinkMode);
unsigned int WINAPI SetCameraStatusEnable(DWORD Enable);
unsigned int WINAPI SetChargeShifting(unsigned int NumberRows, unsigned int NumberRepeats);
unsigned int WINAPI SetComplexImage(int numAreas, int * areas);
VARARGS(SetCoolerMode);
VARARGS(SetCountConvertMode);
VARARGS(SetCountConvertWavelength);
VARARGS(SetCropMode);
unsigned int WINAPI SetCurrentCamera(long cameraHandle);
VARARGS(SetCustomTrackHBin);
VARARGS(SetDataType);
VARARGS(SetDACOutput);
VARARGS(SetDACOutputScale);
unsigned int WINAPI SetDDGAddress(BYTE t0, BYTE t1, BYTE t2, BYTE t3, BYTE address);
unsigned int WINAPI SetDDGExternalOutputEnabled(at_u32 uiIndex, at_u32 uiEnabled);
unsigned int WINAPI SetDDGExternalOutputPolarity(at_u32 uiIndex, at_u32 uiPolarity);
unsigned int WINAPI SetDDGExternalOutputStepEnabled(at_u32 uiIndex, at_u32 uiEnabled);
unsigned int WINAPI SetDDGExternalOutputTime(at_u32 uiIndex, at_u64 uiDelay, at_u64 uiWidth);
VARARGS(SetDDGGain);
VARARGS(SetDDGGateStep);
unsigned int WINAPI SetDDGGateTime(at_u64 uiDelay, at_u64 uiWidth);
VARARGS(SetDDGInsertionDelay);
VARARGS(SetDDGIntelligate);
VARARGS(SetDDGIOC);
VARARGS(SetDDGIOCFrequency);
unsigned int WINAPI SetDDGIOCNumber(unsigned long numberPulses);
unsigned int WINAPI SetDDGIOCPeriod(at_u64 period);
unsigned int WINAPI SetDDGIOCTrigger(at_u32 trigger);
unsigned int WINAPI SetDDGOpticalWidthEnabled(at_u32 uiEnabled);

// DDG Lite functions
unsigned int WINAPI SetDDGLiteGlobalControlByte(unsigned char control);
unsigned int WINAPI SetDDGLiteControlByte(AT_DDGLiteChannelId channel, unsigned char control);
unsigned int WINAPI SetDDGLiteInitialDelay(AT_DDGLiteChannelId channel, float fDelay);
unsigned int WINAPI SetDDGLitePulseWidth(AT_DDGLiteChannelId channel, float fWidth);
unsigned int WINAPI SetDDGLiteInterPulseDelay(AT_DDGLiteChannelId channel, float fDelay);
unsigned int WINAPI SetDDGLitePulsesPerExposure(AT_DDGLiteChannelId channel, at_u32 ui32Pulses);

unsigned int WINAPI SetDDGStepCoefficients(at_u32 mode, double p1, double p2);
unsigned int WINAPI SetDDGWidthStepCoefficients(at_u32 mode, double p1, double p2);
unsigned int WINAPI SetDDGStepMode(at_u32 mode);
unsigned int WINAPI SetDDGWidthStepMode(at_u32 mode);
unsigned int WINAPI SetDDGTimes(double t0, double t1, double t2);
VARARGS(SetDDGTriggerMode);
unsigned int WINAPI SetDDGVariableGateStep(int mode, double p1, double p2);
unsigned int WINAPI SetDelayGenerator(int board, short address, int typ);
unsigned int WINAPI SetDMAParameters(int MaxImagesPerDMA, float SecondsPerDMA);
unsigned int WINAPI SetDriverEvent(HANDLE driverEvent);
VARARGS(SetEMAdvanced);
VARARGS(SetEMCCDGain);
VARARGS(SetEMClockCompensation);
VARARGS(SetEMGainMode);
VARARGS(SetExposureTime);
unsigned int WINAPI SetExternalTriggerTermination(at_u32 uiTermination);
VARARGS(SetFanMode);
VARARGS(SetFastExtTrigger);
unsigned int WINAPI SetFastKinetics(int exposedRows, int seriesLength, float time, int mode, int hbin, int vbin);
unsigned int WINAPI SetFastKineticsEx(int exposedRows, int seriesLength, float time, int mode, int hbin, int vbin, int offset);
unsigned int WINAPI SetSuperKinetics(int exposedRows, int seriesLength, float time, int mode, int hbin, int vbin, int offset);
VARARGS(SetTimeScan);
VARARGS(SetFilterMode);
unsigned int WINAPI SetFilterParameters(int width, float sensitivity, int range, float accept, int smooth, int noise);
VARARGS(SetFKVShiftSpeed);
VARARGS(SetFPDP);
VARARGS(SetFrameTransferMode);
unsigned int WINAPI SetFrontEndEvent(HANDLE driverEvent);
VARARGS(SetFullImage);
VARARGS(SetFVBHBin);
VARARGS(SetGain);
unsigned int WINAPI SetGate(float delay, float width, float stepRenamed);
VARARGS(SetGateMode);
VARARGS(SetHighCapacity);
VARARGS(SetHorizontalSpeed);
VARARGS(SetHSSpeed);

//unsigned int WINAPI SetImage(int hbin, int vbin, int hstart, int hend, int vstart, int vend);
VARARGS(SetImage);

VARARGS(SetImageFlip);
VARARGS(SetImageRotate);

//unsigned int WINAPI SetIsolatedCropMode(int active, int cropheight, int cropwidth, int vbin, int hbin);
VARARGS(SetIsolatedCropMode);

//unsigned int WINAPI SetIsolatedCropModeEx(int active, int cropheight, int cropwidth, int vbin, int hbin, int cropleft, int cropbottom);
VARARGS(SetIsolatedCropModeEx);

VARARGS(SetKineticCycleTime);
VARARGS(SetMCPGain);
VARARGS(SetMCPGating);
unsigned int WINAPI SetMessageWindow(HWND wnd);
VARARGS(SetMetaData);

// unsigned int WINAPI SetMultiTrack(int number, int height, int offset, int * bottom, int * gap);
VARARGS(SetMultiTrack);

VARARGS(SetMultiTrackHBin);
VARARGS(SetMultiTrackHRange);
unsigned int WINAPI SetMultiTrackScan(int trackHeight, int numberTracks, int iSIHStart, int iSIHEnd, int trackHBinning, int trackVBinning, int trackGap, int trackOffset, int trackSkip, int numberSubFrames);
unsigned int WINAPI SetNextAddress(at_32 * data, long lowAdd, long highAdd, long length, long physical);
unsigned int WINAPI SetNextAddress16(at_32 * data, long lowAdd, long highAdd, long length, long physical);
VARARGS(SetNumberAccumulations);
VARARGS(SetNumberKinetics);
VARARGS(SetNumberPrescans);
VARARGS(SetOutputAmplifier);
VARARGS(SetOverlapMode);
VARARGS(SetPCIMode);
VARARGS(SetPhotonCounting);
unsigned int WINAPI SetPhotonCountingThreshold(long min, long max);
unsigned int WINAPI SetPhosphorEvent(HANDLE driverEvent);
unsigned int WINAPI SetPhotonCountingDivisions(at_u32 noOfDivisions, at_32 * divisions);
VARARGS(SetPixelMode);
VARARGS(SetPreAmpGain);
unsigned int WINAPI SetDualExposureTimes(float expTime1, float expTime2);
VARARGS(SetDualExposureMode);

//unsigned int WINAPI SetRandomTracks(int numTracks, int * areas);
SIZE_AND_ARRAY(SetRandomTracks);

VARARGS(SetReadMode);
unsigned int WINAPI SetReadoutRegisterPacking(unsigned int mode);
VARARGS(SetRegisterDump);
unsigned int WINAPI SetRingExposureTimes(int numTimes, float * times);
unsigned int WINAPI SetSaturationEvent(HANDLE saturationEvent);
unsigned int WINAPI SetShutter(int typ, int mode, int closingtime, int openingtime);
unsigned int WINAPI SetShutterEx(int typ, int mode, int closingtime, int openingtime, int extmode);
unsigned int WINAPI SetShutters(int typ, int mode, int closingtime, int openingtime, int exttype, int extmode, int dummy1, int dummy2);
unsigned int WINAPI SetSifComment(char * comment);
VARARGS(SetSingleTrack);
VARARGS(SetSingleTrackHBin);
unsigned int WINAPI SetSpool(int active, int method, char * path, int framebuffersize);
VARARGS(SetSpoolThreadCount);
unsigned int WINAPI SetStorageMode(long mode);
unsigned int WINAPI SetTECEvent(HANDLE driverEvent);
VARARGS(SetTemperature);
unsigned int WINAPI SetTemperatureEvent(HANDLE temperatureEvent);
VARARGS(SetTriggerMode);
VARARGS(SetTriggerInvert);
unsigned int WINAPI GetTriggerLevelRange(float * minimum, float * maximum);
VARARGS(SetTriggerLevel);
VARARGS(SetIODirection);
VARARGS(SetIOLevel);
unsigned int WINAPI SetUserEvent(HANDLE userEvent);
unsigned int WINAPI SetUSGenomics(long width, long height);
VARARGS(SetVerticalRowBuffer);
VARARGS(SetVerticalSpeed);
VARARGS(SetVirtualChip);
VARARGS(SetVSAmplitude);
VARARGS(SetVSSpeed);
NOARGS(ShutDown);
NOARGS(StartAcquisition);
NOARGS(UnMapPhysicalAddress);
NOARGS(UpdateDDGTimings);
NOARGS(WaitForAcquisition);
unsigned int WINAPI WaitForAcquisitionByHandle(long cameraHandle);
unsigned int WINAPI WaitForAcquisitionByHandleTimeOut(long cameraHandle, int iTimeOutMs);
VARARGS(WaitForAcquisitionTimeOut);
unsigned int WINAPI WhiteBalance(WORD * wRed, WORD * wGreen, WORD * wBlue, float * fRelR, float * fRelB, WhiteBalanceInfo * info);

unsigned int WINAPI OA_Initialize(const char * const pcFilename, unsigned int uiFileNameLen);
unsigned int WINAPI OA_EnableMode(const char * const pcModeName);
unsigned int WINAPI OA_GetModeAcqParams(const char * const pcModeName, char * const pcListOfParams);
unsigned int WINAPI OA_GetUserModeNames(char * pcListOfModes);
unsigned int WINAPI OA_GetPreSetModeNames(char * pcListOfModes);
unsigned int WINAPI OA_GetNumberOfUserModes(unsigned int * const puiNumberOfModes);
unsigned int WINAPI OA_GetNumberOfPreSetModes(unsigned int * const puiNumberOfModes);
unsigned int WINAPI OA_GetNumberOfAcqParams(const char * const pcModeName, unsigned int * const puiNumberOfParams);
unsigned int WINAPI OA_AddMode(char * pcModeName, unsigned int uiModeNameLen, char * pcModeDescription, unsigned int uiModeDescriptionLen);
unsigned int WINAPI OA_WriteToFile(const char * const pcFileName, unsigned int uiFileNameLen);
unsigned int WINAPI OA_DeleteMode(const char * const pcModeName, unsigned int uiModeNameLen);
unsigned int WINAPI OA_SetInt(const char * const pcModeName, const char * pcModeParam, const int iIntValue);
unsigned int WINAPI OA_SetFloat(const char * const pcModeName, const char * pcModeParam, const float fFloatValue);
unsigned int WINAPI OA_SetString(const char * const pcModeName, const char * pcModeParam, char * pcStringValue, const unsigned int uiStringLen);
unsigned int WINAPI OA_GetInt(const char * const pcModeName, const char * const pcModeParam, int * iIntValue);
unsigned int WINAPI OA_GetFloat(const char * const pcModeName, const char * const pcModeParam, float * fFloatValue);
unsigned int WINAPI OA_GetString(const char * const pcModeName, const char * const pcModeParam, char * pcStringValue, const unsigned int uiStringLen);

unsigned int WINAPI Filter_SetMode(unsigned int mode);
unsigned int WINAPI Filter_GetMode(unsigned int * mode);
VARARGS(Filter_SetThreshold);
NOARGS(Filter_GetThreshold);
VARARGS(Filter_SetDataAveragingMode);
NOARGS(Filter_GetDataAveragingMode);
VARARGS(Filter_SetAveragingFrameCount);
NOARGS(Filter_GetAveragingFrameCount);
VARARGS(Filter_SetAveragingFactor);
NOARGS(Filter_GetAveragingFactor);

unsigned int WINAPI PostProcessNoiseFilter(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iBaseline, int iMode, float fThreshold, int iHeight, int iWidth);
unsigned int WINAPI PostProcessCountConvert(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iBaseline, int iMode, int iEmGain, float fQE, float fSensitivity, int iHeight, int iWidth);
unsigned int WINAPI PostProcessPhotonCounting(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iNumframes, int iNumberOfThresholds, float * pfThreshold, int iHeight, int iWidth);
unsigned int WINAPI PostProcessDataAveraging(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iAveragingFilterMode, int iHeight, int iWidth, int iFrameCount, int iAveragingFactor);
