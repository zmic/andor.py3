#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h> 
#include <stdarg.h> 

#include <utility>
#include <map>
#include <vector>

#include "ATMCD32D.H"

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif
#include <python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
//#define PY_ARRAY_UNIQUE_SYMBOL SOME_NAME
#include <numpy\arrayobject.h>
#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG 1
#undef _DEBUG_WAS_DEFINED
#endif

#define MODULE_NAME AndorSDK
#define MODULE_NAME_STRING "AndorSDK"

