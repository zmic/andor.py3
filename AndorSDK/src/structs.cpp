#include "include.h"
#include "util.h"
#include "structs.h"

// ###########################################################################################

template<typename pywrappedtype, int pywrappedtype::_T::*field>
PyObject* int_getter(pywrappedtype *obj, void *)
{
	return PyLong_FromLong(obj->_cargo.*field);
}

int _setfield(int* target, PyObject* value)
{
	if (value == NULL)
	{
		PyErr_SetString(PyExc_TypeError, "Cannot delete this attribute");
		return -1;
	}
	*target = PyLong_AsLong(value);
	if (PyErr_Occurred())
	{
		return -1;
	}
	return 0;
}


int _setfield(ULONG* target, PyObject* value)
{
	if (value == NULL)
	{
		PyErr_SetString(PyExc_TypeError, "Cannot delete this attribute");
		return -1;
	}
	*target = PyLong_AsLong(value);
	if (PyErr_Occurred())
	{
		return -1;
	}
	return 0;
}

template<typename pywrappedtype, int pywrappedtype::_T::*field>
int int_setter(pywrappedtype *obj, PyObject* value, void *)
{
	return _setfield(&(obj->_cargo.*field), value);
}

template<typename pywrappedtype, ULONG pywrappedtype::_T::*field>
int ULONG_setter(pywrappedtype *obj, PyObject* value, void *)
{
	return _setfield(&(obj->_cargo.*field), value);
}


#define INT_GETTERSETTER(pywrappedtype, field)\
{ #field, (getter)int_getter<pywrappedtype, &pywrappedtype::_T::field>,(setter)int_setter<pywrappedtype, &pywrappedtype::_T::field>,#field,0 }

#define ULONG_GETTERSETTER(pywrappedtype, field)\
{ #field, (getter)ULONG_getter<pywrappedtype, &pywrappedtype::_T::field>,(setter)ULONG_setter<pywrappedtype, &pywrappedtype::_T::field>,#field,0 }


template<typename pywrappedtype, ULONG pywrappedtype::_T::*field>
PyObject* ULONG_getter(pywrappedtype *obj, void *)
{
	return PyLong_FromUnsignedLong(obj->_cargo.*field);
}
#define ULONG_GETTER(pywrappedtype, field)\
{ #field, (getter)ULONG_getter<pywrappedtype, &pywrappedtype::_T::field>,(setter)0,#field,0 }


// ###########################################################################################



template<class wrappedobject>
void init_type(PyObject *module)
{
	wrappedobject::typeobject.tp_basicsize = sizeof(wrappedobject);
	wrappedobject::typeobject.tp_flags = Py_TPFLAGS_DEFAULT;
	wrappedobject::typeobject.tp_getset = wrappedobject::getsetters;
	wrappedobject::typeobject.tp_new = PyType_GenericNew;	
	//wrappedobject::typeobject.tp_getattro = PyObject_GenericGetAttr;
	//wrappedobject::typeobject.tp_setattro = PyObject_GenericSetAttr;
	PyType_Ready(&wrappedobject::typeobject);
	Py_INCREF(&wrappedobject::typeobject);
	//PyObject_SetAttrString((PyObject*)&wrappedobject::typeobject, "sizeof", pyint(sizeof(wrappedobject::_T)));
	PyModule_AddObject(module, wrappedobject::name, (PyObject *)&wrappedobject::typeobject);
}

// ###########################################################################################


const char* py_AndorCapabilities::name = "AndorCapabilities";
PyGetSetDef py_AndorCapabilities::getsetters[] = {
	ULONG_GETTERSETTER(py_AndorCapabilities,ulSize),
	ULONG_GETTER(py_AndorCapabilities,ulAcqModes),
	ULONG_GETTER(py_AndorCapabilities,ulReadModes),
	ULONG_GETTER(py_AndorCapabilities,ulTriggerModes),
	ULONG_GETTER(py_AndorCapabilities,ulCameraType),
	ULONG_GETTER(py_AndorCapabilities,ulPixelMode),
	ULONG_GETTER(py_AndorCapabilities,ulSetFunctions),
	ULONG_GETTER(py_AndorCapabilities,ulGetFunctions),
	ULONG_GETTER(py_AndorCapabilities,ulFeatures),
	ULONG_GETTER(py_AndorCapabilities,ulPCICard),
	ULONG_GETTER(py_AndorCapabilities,ulEMGainCapability),
	ULONG_GETTER(py_AndorCapabilities,ulFTReadModes),
   { NULL }  /* Sentinel */
};

PyTypeObject py_AndorCapabilities::typeobject = {
	PyVarObject_HEAD_INIT(NULL, 0)
	MODULE_NAME_STRING ".AndorCapabilities",             /* tp_name */
};

// ###########################################################################################

const char* py_ColorDemosaicInfo::name = "ColorDemosaicInfo";
PyGetSetDef py_ColorDemosaicInfo::getsetters[] = {
	INT_GETTERSETTER(py_ColorDemosaicInfo, iX),
	INT_GETTERSETTER(py_ColorDemosaicInfo, iY),
	INT_GETTERSETTER(py_ColorDemosaicInfo, iAlgorithm),
	INT_GETTERSETTER(py_ColorDemosaicInfo, iXPhase),
	INT_GETTERSETTER(py_ColorDemosaicInfo, iYPhase),
	INT_GETTERSETTER(py_ColorDemosaicInfo, iBackground),
{ NULL }  /* Sentinel */
};

PyTypeObject py_ColorDemosaicInfo::typeobject = {
	PyVarObject_HEAD_INIT(NULL, 0)
	MODULE_NAME_STRING ".ColorDemosaicInfo",             /* tp_name */
};

// ###########################################################################################

const char* py_WhiteBalanceInfo::name = "WhiteBalanceInfo";
PyGetSetDef py_WhiteBalanceInfo::getsetters[] = {
	INT_GETTERSETTER(py_WhiteBalanceInfo,iSize),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iX),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iY),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iAlgorithm),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iROI_left),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iROI_right),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iROI_top),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iROI_bottom),
	INT_GETTERSETTER(py_WhiteBalanceInfo,iOperation),
	{ NULL }  /* Sentinel */
};

PyTypeObject py_WhiteBalanceInfo::typeobject = {
	PyVarObject_HEAD_INIT(NULL, 0)
	MODULE_NAME_STRING ".WhiteBalanceInfo",             /* tp_name */
};

// ###########################################################################################

void init_structs(PyObject* module)
{
	init_type<py_AndorCapabilities>(module);
	init_type<py_ColorDemosaicInfo>(module);
	init_type<py_WhiteBalanceInfo>(module);
}
