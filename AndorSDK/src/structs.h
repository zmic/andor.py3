void init_structs(PyObject* module);

template<typename T>
struct PyWrappedObject
{
	typedef T _T;
	PyObject_HEAD
		T _cargo;
};

struct py_AndorCapabilities : PyWrappedObject<AndorCapabilities>
{
	static const char* name;
	static PyGetSetDef getsetters[];
	static PyTypeObject typeobject;
};

struct py_ColorDemosaicInfo : PyWrappedObject<ColorDemosaicInfo>
{
	static const char* name;
	static PyGetSetDef getsetters[];
	static PyTypeObject typeobject;
};

struct py_WhiteBalanceInfo : PyWrappedObject<WhiteBalanceInfo>
{
	static const char* name;
	static PyGetSetDef getsetters[];
	static PyTypeObject typeobject;
};
