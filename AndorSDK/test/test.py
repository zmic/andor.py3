from __future__ import print_function
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt    

def detect_platform():
    ''' 
    return value: one of
        "x86"    =>  32 bit python on 32 bit windows
        "amd64"  =>  64 bit python on 64 bit windows
        "wow64"  =>  32 bit python on 64 bit windows (32 bit emulation mode)
        
        
    see: http://blogs.msdn.com/b/david.wang/archive/2006/03/26/howto-detect-process-bitness.aspx
        
                               32bit Native     64bit Native     WOW64
    PROCESSOR_ARCHITECTURE         "x86"          "AMD64"        "x86"
    PROCESSOR_ARCHITEW6432       undefined       undefined      "AMD64"        
    '''
    
    platform = os.environ['PROCESSOR_ARCHITECTURE']
    if platform == "AMD64": 
        return "amd64" 
    elif platform == "x86":
        try:
            platform = os.environ["PROCESSOR_ARCHITEW6432"]
        except KeyError:
            return "x86"
        if platform == "AMD64":
            return "wow64"
        else:
            raise RuntimeError("Environment variable PROCESSOR_ARCHITEW6432 has unexpected value: %r"%platform)
    else:
        raise RuntimeError("Environment variable PROCESSOR_ARCHITECTURE has unexpected value: %r"%platform)
        
    
if detect_platform() == 'amd64':
    folder = r"D:\dev\Andor\vs2015\x64\Debug-%d%d"%(sys.version_info[0],sys.version_info[1])
else:
    folder = r"D:\dev\Andor\vs2015\Debug-%d%d"%(sys.version_info[0],sys.version_info[1])
    
sys.path.append(folder)
os.environ['path'] += r";C:\Program Files\Andor SOLIS\Drivers"
 
import AndorSDK as a

AC_SETFUNCTION_VREADOUT = 0x01
AC_SETFUNCTION_HREADOUT = 0x02
AC_SETFUNCTION_TEMPERATURE = 0x04
AC_SETFUNCTION_MCPGAIN = 0x08
AC_SETFUNCTION_EMCCDGAIN = 0x10
AC_SETFUNCTION_BASELINECLAMP = 0x20
AC_SETFUNCTION_VSAMPLITUDE = 0x40
AC_SETFUNCTION_HIGHCAPACITY = 0x80
AC_SETFUNCTION_BASELINEOFFSET = 0x0100
AC_SETFUNCTION_PREAMPGAIN = 0x0200
AC_SETFUNCTION_CROPMODE = 0x0400
AC_SETFUNCTION_DMAPARAMETERS = 0x0800
AC_SETFUNCTION_HORIZONTALBIN = 0x1000
AC_SETFUNCTION_MULTITRACKHRANGE = 0x2000
AC_SETFUNCTION_RANDOMTRACKNOGAPS = 0x4000
AC_SETFUNCTION_EMADVANCED = 0x8000
AC_SETFUNCTION_GATEMODE = 0x010000
AC_SETFUNCTION_DDGTIMES = 0x020000
AC_SETFUNCTION_IOC = 0x040000
AC_SETFUNCTION_INTELLIGATE = 0x080000
AC_SETFUNCTION_INSERTION_DELAY = 0x100000
AC_SETFUNCTION_GATESTEP = 0x200000
AC_SETFUNCTION_GATEDELAYSTEP = 0x200000
AC_SETFUNCTION_TRIGGERTERMINATION = 0x400000
AC_SETFUNCTION_EXTENDEDNIR = 0x800000
AC_SETFUNCTION_SPOOLTHREADCOUNT = 0x1000000
AC_SETFUNCTION_REGISTERPACK = 0x2000000
AC_SETFUNCTION_PRESCANS = 0x4000000
AC_SETFUNCTION_GATEWIDTHSTEP = 0x8000000
AC_SETFUNCTION_EXTENDED_CROP_MODE = 0x10000000




def test1():
   
    assert a.DRV_IDLE == 20073

    a.Initialize("")
    # This function returns the size of the detector in pixels
    xpix, ypix = a.GetDetector()
    print("xpix, ypix:", xpix, ypix)
    
    caps = a.AndorCapabilities()
    caps.ulSize = 48
    a.GetCapabilities(caps)
    
    # 1 Single Scan 
    # 2 Accumulate 
    # 3 Kinetics 
    # 4 Fast Kinetics 
    # 5 Run till abort
    a.SetAcquisitionMode(1)
    
    # 0 Full Vertical Binning 
    # 1 Multi-Track 
    # 2 Random-Track 
    # 3 Single-Track 
    # 4 Image
    a.SetReadMode(0)
    
    #print(caps.ulSetFunctions) 
    if caps.ulSetFunctions & AC_SETFUNCTION_TEMPERATURE:
        pass
    mintemp, max_temp = a.GetTemperatureRange()
    print("mintemp, max_temp:",mintemp, max_temp)
    if(not a.IsCoolerOn()):
        print("Switch on cooler")
        a.CoolerON()
    a.SetTemperature(-80)
    while 1:
        time.sleep(1)
        t = a.GetTemperature()
        print("GetTemperature:", a.GetTemperature())
        if t < -70:
            break
    
    ###################################################################################################
    
    # This function will set the exposure time to the nearest valid value not less than
    # the given value. The actual exposure time used is obtained by GetAcquisitionTimings.
    for et in np.arange(0.1,1,0.2):
        a.SetExposureTime(et)
        
        a.StartAcquisition()
        while 1:
            time.sleep(.025)
            status = a.GetStatus()
            print(status)
            if status == a.DRV_IDLE:
                break
        A = np.zeros( (xpix,), dtype = np.int32)            
        a.GetAcquiredData(A, xpix)
        x = range(len(A))
        plt.plot(x, A)
        plt.show()
        
    ###################################################################################################
    print("Switch off cooler")

    a.CoolerOFF()
    while 1:
        time.sleep(1)
        t = a.GetTemperature()
        print("GetTemperature:", a.GetTemperature())
        if t > -20:
            break
    print("shutting down...")
    a.ShutDown()
    
    return

        

def test2():
   
    a.Initialize("")
    ###################################################################################################
    print("Switch off cooler")

    a.CoolerOFF()
    while 1:
        time.sleep(1)
        t = a.GetTemperature()
        print("GetTemperature:", a.GetTemperature())
        if t > -20:
            break
    print("shutting down...")
    a.ShutDown()

    
    
    
test2()

    
    
    





